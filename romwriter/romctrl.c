/*
 * romctrl.c
 *
 *  Created on: 2016/08/04
 *      Author: �@��Y
 */
#include <avr/io.h>
#include <util/delay.h>
#include "romctrl.h"

int romkind=ROMMODE_NONE;


void databus_outdir(void){
	DDRC|=0x3f;
	DDRD|=0x0c;
}
void databus_indir(void){
	DDRC&=~0x3f;
	DDRD&=~0x0c;
}

void databus_set(unsigned char data){
	PORTC&=~(0x3f);
	PORTC|=0x3f&data;
	PORTD&=~(0x0c);
	PORTD|=0x0c&(data>>4);
}
unsigned char databus_read(void){
	unsigned char data=0;
	data|=0x3f&PINC;
	data|=(PIND<<4)&0xc0;
	return data;
}

void addrbus_enable(void){
	PORTD&=~(1<<5);
}
void addrbus_disable(void){
	PORTD|=(1<<5);
}
void addr_set(unsigned int addr){
	unsigned char addrh=0xff&(addr>>8);
	unsigned char addrl=0xff&addr;
	PORTD&=~(1<<4);
	PORTB&=~(0x01);
	databus_set(addrl);
	PORTB|=0x01;
	PORTB&=~0x01;

	unsigned char dbus=0;
	int bitmap[8]={7,2,3,1,0,5,6,4};
	int i;
	for(i=0;i<8;i++){
		if(addrh&(1<<bitmap[i])){
			dbus|=(1<<i);
		}
	}
	databus_set(dbus);
	PORTD|=(1<<4);
	PORTD&=~(1<<4);
	if(romkind==ROMMODE_EEP){
		if(addrh&(1<<6)){
			PORTB|=1<<4;
		}else{
			PORTB&=~(1<<4);
		}
	}else if(romkind==ROMMODE_EP){
		if(addrh&(1<<6)){
			PORTD|=1<<6;
		}else{
			PORTD&=~(1<<6);
		}
	}
}

void rom_we_assert(void){//EEPROM-> WE->0
	if(romkind==ROMMODE_EEP){
		PORTD&=~(1<<6);
	}
}
void rom_we_negate(void){//EEPROM-> WE->1,EPROM ->Vpp=5
	if(romkind==ROMMODE_EEP){
		PORTD|=(1<<6);
	}
}
void rom_oe_assert(void){
	PORTD&=~(1<<7);
}
void rom_oe_negate(void){
	PORTD|=(1<<7);
}
void rom_ce_assert(void){
	PORTB&=~(1<<3);
	DDRB|=(1<<3);
}
void rom_ce_negate(void){
	PORTB|=(1<<3);
}
void rom_ce_hiz(void){
	DDRB&=~(1<<3);
}

void eprom_power_on(void){//Apply vcc to vcc and vpp pin
	romkind=ROMMODE_EP;
	rom_ce_hiz();
	PORTB|=(1<<2);
	PORTB|=(1<<4);
	PORTB&=~(1<<1);
	addrbus_enable();
	rom_ce_negate();
	rom_oe_negate();

}
void eprom_power_off(void){//cut vcc to vcc and vpp pin
	rom_ce_negate();
	rom_ce_hiz();
	rom_ce_assert();//set zero
	rom_oe_assert();//set ctrl pin zero
	rom_we_assert();
	addrbus_disable();
	PORTB|=(1<<1);
	PORTB&=~(1<<4);
	PORTB|=(1<<2);
	romkind=ROMMODE_NONE;
}
void eeprom_power_on(void){
	romkind=ROMMODE_EEP;
	rom_ce_hiz();
	PORTB|=(1<<2);
	PORTB&=~(1<<1);
	rom_ce_negate();
	addrbus_enable();
	rom_we_negate();
	rom_oe_negate();
}
void eeprom_power_off(void){

	rom_ce_negate();
	rom_ce_hiz();
	rom_ce_assert();
	rom_oe_assert();//set ctrl pin zero
	rom_we_assert();
	addrbus_disable();
	PORTB|=(1<<1);
	PORTB|=(1<<2);
	romkind=ROMMODE_NONE;
}

void eprom_apply_vpp(void){
	PORTB&=~(1<<2);
}
void eprom_remove_vpp(void){
	PORTB|=(1<<2);
}

void port_init(void){
	DDRC|=0x3f;
	DDRD|=0xfc;
	DDRB|=0x1f;
	eprom_power_off();
	eeprom_power_off();
	addrbus_disable();
	databus_indir();
	rom_ce_hiz();
	eprom_remove_vpp();
}


unsigned char rom_read_byte(unsigned int addr){//normal read
	rom_ce_negate();
	rom_oe_negate();
	rom_we_negate();
	databus_outdir();
	addr_set(addr);
	databus_indir();
	addrbus_enable();
	rom_ce_assert();
	rom_oe_assert();
	_delay_us(1);
	unsigned char readval=databus_read();
	rom_oe_negate();
	rom_ce_negate();
	return readval;
}

int eprom_verify(unsigned int addr,unsigned char data){
	rom_ce_negate();
	rom_oe_negate();
	databus_outdir();
	addr_set(addr);
	databus_indir();
	rom_oe_assert();
	_delay_us(1);
	unsigned char rdata=databus_read();
	rom_oe_negate();
	if(data==rdata){
		return 1;
	}else{
		return -1;
	}
}
int eprom_write_byte(unsigned int addr,unsigned char data){
	rom_ce_negate();
	rom_oe_negate();
	eprom_apply_vpp();
	databus_outdir();
	addr_set(addr);
	addrbus_enable();
	int retry_count=0;
	for(retry_count=0;retry_count<25;retry_count++){
		databus_outdir();
		databus_set(data);
		rom_ce_assert();
		_delay_us(100);
		rom_ce_negate();
		_delay_us(1);
		databus_indir();
		if(eprom_verify(addr,data)==1){//succeed
			break;
		}

	}

	eprom_remove_vpp();
	if(retry_count==25){
		return -1;
	}else{
		return 1;
	}
}

int eeprom_write_wait(unsigned int addr,unsigned char data){
	int timecount=0;
	for(timecount=0;timecount<20;timecount++){
		_delay_ms(1);
		if(data==rom_read_byte(addr)){
			break;
		}
	}
	if(timecount==20){
		return -1;
	}else{
		return 1;
	}
}
int eeprom_write_byte(unsigned int addr,unsigned char data){
	rom_ce_negate();
	rom_oe_negate();
	rom_we_negate();
	databus_outdir();
	addr_set(addr);
	addrbus_enable();
	databus_set(data);
	_delay_us(1);
	rom_ce_assert();
	rom_we_assert();
	_delay_us(1);
	rom_we_negate();
	return eeprom_write_wait(addr,data);
}
int eeprom_write_page(unsigned int addr,unsigned char*data,unsigned int len){
	rom_ce_negate();
	rom_oe_negate();
	rom_we_negate();
	int i;
	unsigned char verify_dat=*(data+len-1);
	for(i=0;i<len;i++){
		databus_outdir();
		addr_set(addr+i);
		addrbus_enable();
		databus_set(*(data+i));
		_delay_us(1);
		rom_ce_assert();
		rom_we_assert();
		_delay_us(1);
		rom_we_negate();
	}
	_delay_us(100);
	return eeprom_write_wait(addr+len-1,verify_dat);
}
int rom_read(unsigned int addr ,unsigned char*readdata,unsigned int len){
	int readcount=0;
	unsigned char rdata;
	for(readcount=0;readcount<len;readcount++){
		if(romkind==ROMMODE_EP){
			rdata=rom_read_byte(addr+readcount);
		}else if(romkind==ROMMODE_EEP){
			rdata=rom_read_byte(addr+readcount);
		}else {
			break;
		}
		*(readdata+readcount)=rdata;
	}
	return readcount;
}
int rom_write(unsigned int addr ,unsigned char*writedata,unsigned int len){
	int writecount=0;
	unsigned char wdata;
	if(romkind==ROMMODE_EP){
		for(writecount=0;writecount<len;writecount++){
			wdata=*(writedata+writecount);
			if(eprom_write_byte(addr+writecount,wdata)==-1){
				break;
			}
		}
	}else if(romkind==ROMMODE_EEP){
		writecount=0;
		int result=0;
		while(writecount<len){
			int blocksize=0x40-((addr+writecount)&0x3f);
			int remain_size=len-writecount;
			if(blocksize>remain_size){
				blocksize=remain_size;
			}
			if(blocksize==1){
				result=eeprom_write_byte(addr+writecount,*(writedata+writecount));
			}else{
				result=eeprom_write_page(addr+writecount,writedata+writecount,blocksize);
			}
			if(result==-1){
				break;
			}
			writecount+=blocksize;
		}
	}
	return writecount;
}


