/*
 * main.c
 *
 *  Created on: 2016/08/04
 *      Author: �@��Y
 */

#include <avr/io.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "uart.h"
#include "romctrl.h"

void init(void){
	uart_init(br57600);
	uart_bind_stdio();
	port_init();
}
char* err_mesg[4]={"Invalid CMD","Checksum Error","Too long","Verify Fail"};
enum ErrKind{Invalid_CMD=0,Chksum_ERR=1,Too_long=2,Verify_fail=3};
int debug=0;
void print_accept(){
	printf("OK\n");
}
void print_error(int errorcord){
	printf("Error %s\n",err_mesg[errorcord]);
}
void rom_read_and_print(int addr,int len);
void hex_recv_and_write(void);
char line[81];
int main(void){
	init();

	int romkind=ROMMODE_NONE;
	printf("ROM WRITER\n");
	printf("READY\n");
	for(;;){
		fgets(line,sizeof(line),stdin);
		strtok(line," \n\r");
		if(strcmp(line,"Debug")==0){
			debug=1;
		}
		if(strcmp(line,"27C256_ON")==0){
			if(romkind==ROMMODE_NONE){
				eprom_power_on();
				romkind=ROMMODE_EP;
				print_accept();
			}else{
				print_error(Invalid_CMD);
			}
		}
		if(strcmp(line,"58C256_ON")==0){
				if(romkind==ROMMODE_NONE){
				eeprom_power_on();
				romkind=ROMMODE_EEP;
				print_accept();
			}else{
				print_error(Invalid_CMD);
			}
		}
		if(strcmp(line,"OFF")==0){
			if(romkind==ROMMODE_EP){
				eprom_power_off();
				romkind=ROMMODE_NONE;
				print_accept();
			}else if(romkind==ROMMODE_EEP){
				eeprom_power_off();
				romkind=ROMMODE_NONE;
				print_accept();
			}
		}
		if(strcmp(line,"WRITE")==0){
			if(romkind==ROMMODE_NONE){
				print_error(Invalid_CMD);
			}else{
				print_accept();
				hex_recv_and_write();
			}
		}
		if(strcmp(line,"READ")==0){//READ addr len\n\r
			if(romkind==ROMMODE_NONE){
				print_error(Invalid_CMD);
			}else{
				if(debug){
					printf("Read Operation\n\r");
				}
				int addr=0;
				int len=0;
				char*addrstr=strtok(0," ");
				char*lenstr=strtok(0," ");
				if(debug){
					printf("Addr_str=%s,Len_str=%s\n\r",addrstr,lenstr);
				}
				addr=strtol(addrstr,0,0);
				len=strtol(lenstr,0,0);
				if(debug){
					printf("Addr:%x,len:%x\n\r",addr,len);
				}
				rom_read_and_print(addr,len);
			}
		}

	}
}

void rom_read_and_print(int addr,int len){//hex record format
	int chksum=addr+len;
	printf(":%02x%04x00",len,addr);//print hex record head
	int i=0;
	unsigned char byte;
	for(i=0;i<len;i++){
		byte=rom_read_byte(addr+i);
		chksum+=byte;
		printf("%02x",byte);
	}
	printf("%02x\n",(0x100-chksum)&0xff);
}
int gethex(char*str,int start,int len){
	char hexstr[5];
	if(len>4){
		return 0;
	}
	strncpy(hexstr,str+start,len);
	return strtol(hexstr,0,16);
}
void hex_recv_and_write(void){
	unsigned char wbuf[64];
	printf("Hex file ready\n\r");
	while(1){
		int len=0;
		int addr=0;
		int rec_type=0;
		int chksum=0;
		int chksum_r=0;
		fgets(line,sizeof(line),stdin);
		strtok(line,"\n\r");
		if(line[0]!=':'){
			if(line[0]=='q'){//quit
				print_accept();
				return;
			}
			print_error(Invalid_CMD);
			continue;
		}
		len=gethex(line,1,2);
		chksum_r+=len;
		if(len>sizeof(wbuf)){
			print_error(Too_long);
			continue;
		}
		addr=gethex(line,3,4);
		chksum_r+=(addr&0xff)+((addr>>8)&0xff);
		rec_type=gethex(line,7,2);
		if(rec_type==0x01){//end hex
			print_accept();
			break;
		}
		chksum_r+=rec_type;
		int i=0;
		for(i=0;i<len;i++){
			wbuf[i]=gethex(line,9+2*i,2);
			chksum_r+=wbuf[i];
		}
		chksum=gethex(line,9+2*len,2);
		if(((chksum+chksum_r)&0xff)!=0){
			print_error(Chksum_ERR);
			continue;
		}
		printf("\x13");//Xoff
		if(len!=rom_write(addr,wbuf,len)){
			print_error(Verify_fail);
		}else{
			print_accept();
		}
		printf("\x11");//Xon
	}
}
