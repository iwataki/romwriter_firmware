/*
 * romctrl.h
 *
 *  Created on: 2016/08/04
 *      Author: �@��Y
 */

#ifndef ROMCTRL_H_
#define ROMCTRL_H_

#define ROMMODE_EP 1
#define ROMMODE_EEP 2
#define ROMMODE_NONE 0
void port_init(void);
void eprom_power_on(void);
void eprom_power_off(void);
void eeprom_power_on(void);
void eeprom_power_off(void);

unsigned char rom_read_byte(unsigned int addr);
int eprom_write_byte(unsigned int addr,unsigned char data);
int eeprom_write_byte(unsigned int addr,unsigned char data);

int rom_read(unsigned int addr ,unsigned char*readdata,unsigned int len);
int rom_write(unsigned int addr ,unsigned char*writedata,unsigned int len);





#endif /* ROMCTRL_H_ */
