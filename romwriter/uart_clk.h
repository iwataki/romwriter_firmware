#ifndef __UART_CLK_H__
#define __UART_CLK_H__

#ifndef UCSR0A
#define UCSR0A UCSRA
#define UCSR0B UCSRB
#define UCSR0C UCSRC
#define UBRR0H UBRRH
#define UBRR0L UBRRL
#define RXC0 RXC
#define TXC0 TXC
#define UDRE0 UDRE
#define FE0 FE
#define DOR0 DOR
#define UPE0 UPE
#define U2X0 U2X
#define MPCM0 MPCM
#define RXCIE0 RXCIE
#define TXCIE0 TXCIE
#define UDRIE0 UDRIE
#define RXEN0 RXEN
#define TXEN0 TXEN
#define UDR0 UDR
#endif

#if (F_CPU==8000000UL)

typedef enum{
	br600	= 1664,
	br1200	= 832, 
	br2400	= 416,
	br4800	= 207,
	br9600	= 103,
	br14400	= 68,
	br19200	= 51,
	br28800	= 34,
	br38400	= 25,
	br57600 = 16
} TBaudRate;
#elif (F_CPU==14745600UL)
// SYSCLOCK 14.7456MHz
typedef enum {
  br2400   = 767,
  br4800   = 383,
  br9600   = 191,
  br14400  = 127,
  br19200  = 95,
  br28800  = 63,
  br38400  = 47,
  br57600  = 31,
  br115200 = 15,
  br230400 = 7,
  br460800 = 3,
  br921600 = 1,
} TBaudRate;
#elif (F_CPU==16000000UL)
// SYSCLOCK 16MHz
typedef enum {
  br600 = 3328,
  br1200 = 1664,
  br2400   = 832,
  br4800   = 416,
  br9600   = 207,
  br14400  = 138,
  br19200  = 103,
  br28800  = 68,
  br38400  = 51,
  br57600  = 34,
  br115200 = 16,
  br200k   = 9,
  br250k   = 7,
  br400k   = 4,
  br500k   = 3,
  br1M     = 1,
} TBaudRate;
#elif (F_CPU==20000000UL)
// SYSCLOCK 20MHz
typedef enum {
  br2400   = 1041,
  br4800   = 520,
  br9600   = 259,
  br14400  = 173,
  br19200  = 129,
  br28800  = 86,
  br38400  = 64,
  br57600  = 42,
  br76800 = 32,
  br115200   = 21,
  br230400   = 10,
  br250k   = 9,
  br500k   = 4,
} TBaudRate;
#else
#error Given F_CPU not supported.
#endif

#endif
