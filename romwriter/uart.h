#ifndef __UART_H__
#define __UART_H__ 1

#include <stdio.h>
#include "uart_clk.h"

void uart_init(TBaudRate b);//初期化用　ビットレートをbr〇〇といった形で設定

void uart_putc(uint8_t c);
unsigned char uart_getc(void);//受信するまで待つ
uint8_t uart_getc_noblock(void);//待たない
void uart_rxbuf_flush(void);//受信バッファを空に

uint8_t uart_is_recieved(void);//受信データ有るとき１

int uart_putchar(char c, FILE *f);
int uart_getchar(FILE *f);

uint8_t uart_put_ready(void);//
uint8_t uart_get_ready(void);

void uart_bind_stdio(void);//これを初期化時に実行するとprintfでシリアルへ出力できる
uint8_t uart_status;
#endif
