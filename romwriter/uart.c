#include <avr/io.h>
#include <stdint.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include "uart.h"

#ifndef USE_FDEVOPEN
static FILE mystdout = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);
static FILE mystdin = FDEV_SETUP_STREAM(NULL, uart_getchar, _FDEV_SETUP_READ);
#endif
struct{
	volatile unsigned char buf[0xff+1]; /*RX ring buffer*/
	volatile unsigned char wp;
	volatile unsigned char rp;
}rx;
struct{
	volatile unsigned char buf[0xff+1]; /*TX ring buffer*/
	volatile unsigned char wp;
	volatile unsigned char rp;
}tx;
void uart_init(TBaudRate b){
  UBRR0H = (b>>8) & 0xff;	/* Setting UART prescaling value */
  UBRR0L = b & 0xff;
  UCSR0A = (1<<U2X0);		/* Configuring UART prescaler to operate at
				 F_CPU * 2 */
  //UCSR0C|=0x20;//Even parity
  UCSR0B = (1<<RXEN0)|(1<<TXEN0)|(1<<RXCIE0)|(1<<UDRIE0); /* Enabling UART receiver and transmitter */
  rx.wp=0;
  rx.rp=0;
  tx.wp=0;
  tx.rp=0;
  sei();
}
ISR(USART_RX_vect){
	uart_status=UCSR0A&0x1c;
	rx.buf[rx.wp++]=UDR0;
}
ISR(USART_UDRE_vect){
	if(tx.rp!=tx.wp){	/*If there are data in tx buffer*/
		UDR0=tx.buf[tx.rp++];
	}else{
		UCSR0B &=~(1<<UDRIE0);
	}
}
void uart_putc(uint8_t c){
	//while ( !( UCSR0A & (1<<UDRE0)) ); /* Waiting for previous transmission to complete */
 	//UDR0 = c;			/* Loading the transmitter with data  */
	tx.buf[tx.wp++]=c;
	UCSR0B |=(1<<UDRIE0);
}
unsigned char uart_getc(void){
  //while ( !(UCSR0A & (1<<RXC0)) ); /* Waiting for data */
  //return UDR0;			/* Return received data */
  	uint8_t data;
	while(rx.rp==rx.wp);
	data= rx.buf[rx.rp++];
	return data;
}

uint8_t uart_getc_noblock(void){
	return rx.buf[rx.rp++];
}

uint8_t uart_is_recieved(void){
	if(rx.rp!=rx.wp){
		return 1;
	}else{
		return 0;
	}
}
void uart_rxbuf_flush(void){
	rx.rp=0;
	rx.wp=0;
}
int uart_putchar(char c, FILE *f){
  if(c=='\n')
    uart_putc('\r');
  uart_putc(c);
  return 0;
}

int uart_getchar(FILE *f){
  unsigned char c;
  c=uart_getc();
  if(c=='\r')
    uart_putc('\n');
  uart_putc(c);
  return c;
}

uint8_t uart_put_ready(){
  return !( !(UCSR0A & (1<<UDRE0)));
}

uint8_t uart_get_ready(){
  return !( !(UCSR0A & (1<<RXC0)));
}
void uart_bind_stdio(){
#ifdef USE_FDEVOPEN
  fdevopen(uart_putchar, uart_getchar);
#else
  stdin = &mystdin;
  stdout = &mystdout;
#endif

}
